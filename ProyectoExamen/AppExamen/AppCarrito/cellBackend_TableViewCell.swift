//
//  cellBackend_TableViewCell.swift
//  AppExamen
//
//  Created by BryanJarrin on 15/6/17.
//  Copyright © 2017 BryanJarrin. All rights reserved.
//

import UIKit

class cellBackend_TableViewCell: UITableViewCell {

    @IBOutlet weak var nameHeroLabel: UILabel!
    
    @IBOutlet weak var heroImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
