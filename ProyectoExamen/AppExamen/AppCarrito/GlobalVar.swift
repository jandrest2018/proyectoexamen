//
//  GlobalVar.swift
//  AppExamen
//
//  Created by JoseTinajero on 12/6/17.
//  Copyright © 2017 JoseTinajero. All rights reserved.
//

import Foundation
import UIKit

var heroesArray = [SHeroe]()

var heroesImageDict = [String: UIImage]()



var heroesArrayBackEnd = [HeroeBackEnd]()

class HeroeBackEnd {
    var idAPI:Int?
    var nombre:String?
    var img:UIImage?
    
    init(idAPI:Int,nombre:String,img:UIImage){
        self.idAPI = idAPI
        self.nombre = nombre
        self.img = img
    }
}
