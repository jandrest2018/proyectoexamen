//
//  ObjectMapper.swift
//  AppExamen
//
//  Created by JoseTinajero on 12/6/17.
//  Copyright © 2017 JoseTinajero. All rights reserved.
//

import Foundation
import ObjectMapper


class SHeroesApiResponse:Mappable{
    var resultado:SHeroesArrayResult?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        resultado <- map["result"]
    }
}

class SHeroesArrayResult:Mappable{
    var heroes:[SHeroe]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        heroes <- map["heroes"]
    }
}

class SHeroe:Mappable{
    var id:Int?
    var nombre:String?
    
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        nombre <- map["name"]
        nombre!.removeSubrange(nombre!.startIndex..<nombre!.index(nombre!.startIndex, offsetBy: 14))
    }
}

