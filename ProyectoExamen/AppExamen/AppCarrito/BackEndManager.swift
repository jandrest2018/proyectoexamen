//
//  BackEndManager.swift
//  AppExamen
//
//  Created by BryanJarrin on 15/6/17.
//  Copyright © 2017 BryanJarrin. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper


class BackEndManager {
    
    
    func getHeroes() {
        let url = "https://api.steampowered.com/IEconDOTA2_570/GetHeroes/v0001/?key=3F8783E282D470C5FCD45C2DF88A8220"
        
        Alamofire.request(url).responseObject { (response: DataResponse<SHeroesApiResponse>) in
            
            let heroesResponse = response.result.value
        
            if let heroesArr = heroesResponse?.resultado {
                heroesArray = heroesArr.heroes!
                 NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object: nil)
            }
        }
    }
    
    func getImageHeroes(_ id:Int){
        let url = "http://cdn.dota2.com/apps/dota2/images/heroes/\(heroesArray[id].nombre!)_lg.png"
        
        Alamofire.request(url).responseImage { response in
            
            guard let image = response.result.value else {
                return
            }
            heroesImageDict[heroesArray[id].nombre!] = image
            
        }
    }
    
    func addHeroToBackEnd(_ heroe:SHeroe)  {
        let url = "http://localhost:1337/Heroe/create?idAPI=\(heroe.id!)&nombre=\(heroe.nombre!)&rutaImagen=http://cdn.dota2.com/apps/dota2/images/heroes/\(heroe.nombre!)_lg.png"

        
        Alamofire.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:
                heroesArrayBackEnd.append(HeroeBackEnd(idAPI: heroe.id!, nombre: heroe.nombre!, img: heroesImageDict[heroe.nombre!]!))
                NotificationCenter.default.post(name: NSNotification.Name("actualizarBackEnd"), object: nil)
                //actualizar la tabla
                
                print("exito")
            case .failure:
                print("error")

            }
        }
        
        
    }
    
    func getHeroesFromBackEnd(){
        Alamofire.request("http://localhost:1337/Heroe/find").responseJSON { response in
            
            if let JSON = response.result.value {
                
                let arrayJSON = JSON as! NSArray;
                
                for i in arrayJSON {
                    let dict = i as! NSDictionary
                    let id = dict["idAPI"]! as! Int
                    let nombre = dict["nombre"]! as! String
                    let ruta = dict["rutaImagen"]! as! String
                    Alamofire.request(ruta).responseImage { response in
                        guard let image = response.result.value else {
                            return
                        }
                        heroesArrayBackEnd.append(HeroeBackEnd(idAPI: id, nombre: nombre, img: image))
                        NotificationCenter.default.post(name: NSNotification.Name("actualizarBackEnd"), object: nil)
  
                    }
                }
                
            }
            
        }
    }
    
    
    
}
