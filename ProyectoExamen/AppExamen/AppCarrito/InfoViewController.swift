//
//  InfoViewController.swift
//  AppExamen
//
//  Created by JoseTinajero on 14/6/17.
//  Copyright © 2017 JoseTinajero. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var imgInfo: UIImageView!
    var index:Int = 0
    let bm = BackEndManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func agregarBackEndPressed(_ sender: Any) {
        
        var alert:UIAlertController
        
        bm.addHeroToBackEnd(heroesArray[index])
        
//        if bm.addHeroToBackEnd(heroesArray[index]) {
            alert = UIAlertController(title: "Notificacion", message: "Se ha agregado con exito", preferredStyle: UIAlertControllerStyle.alert)
            
//        } else {
//             alert = UIAlertController(title: "Notificacion", message: "Heroe ya agregado ", preferredStyle: UIAlertControllerStyle.alert)
//        }
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert,animated:true,completion:nil)
        
        
   
    }
    
    override func viewDidAppear(_ animated: Bool) {
        idLabel.text =  "\(heroesArray[index].id!   )"
        nombreLabel.text = heroesArray[index].nombre
        imgInfo.image = heroesImageDict[nombreLabel.text!]
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
