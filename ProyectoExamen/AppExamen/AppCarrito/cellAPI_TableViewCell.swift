//
//  cellAPI_TableViewCell.swift
//  AppExamen
//
//  Created by JoseTinajero on 12/6/17.
//  Copyright © 2017 JoseTinajero. All rights reserved.
//

import UIKit

class cellAPI_TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameHeroLabel: UILabel!
    
    @IBOutlet weak var addToCarButton: UIButton!
    
    @IBOutlet weak var heroImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
